﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour
{
    public GameObject painQuestion, fim;
    public float speed;
    public Transform target;
    public Transform origin;
    public Transform ring1;
    public UnityEvent restart;
    public RingSpawner ringSpawner;
    public int score = 0;
    public int totalPoints = 0;
    public GameManager gameManager;
    
    private bool _move = false;
    private Text _scoreText;
    private Text _highScoreText;
    private int _highScore = 0;
    private SpriteRenderer _arrow;
    private AudioSource _audioSource;
    private int _deathCount = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        _scoreText = GameObject.FindGameObjectWithTag("Score").GetComponent<Text>();
        _highScoreText = GameObject.FindGameObjectWithTag("HighScore").GetComponent<Text>();
        _arrow = GameObject.FindGameObjectWithTag("Arrow").GetComponentInChildren<SpriteRenderer>();
        _arrow.enabled = false;
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (painQuestion.activeSelf == false && fim.activeSelf == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _arrow.enabled = true;

            }
            if (Input.GetMouseButtonUp(0))
            {
                _arrow.enabled = false;
                _move = true;
            }
        }
        
        if (_move)
        {
            transform.position = Vector3.MoveTowards(transform.position,target.position,speed * Time.deltaTime);
        }
        else
        {
            Vector3 vectorToTarget = target.position - transform.position; //gets a vector pointing towards my current target
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speed);
        }

        if (transform.position == target.position)
        {
            _move = false;
            target = target.GetComponent<RingBehaviour>().targetring.GetComponent<Transform>();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            _deathCount++;
            if (_deathCount != 0 && _deathCount%5 == 0)
            {
                SceneManager.LoadScene("Pain");
            }
            _move = false;
            StartCoroutine(Death());
        }
    }

    private IEnumerator Death()
    {
        _audioSource.Play();
        yield return new WaitForSeconds(1);
        transform.position = origin.position;
        score = 0;
        _scoreText.text = "" + score;
        target = ring1;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ring") && other.gameObject.name != "Origin")
        {
            score++;
            totalPoints++;
            if (score != 0 && score%3 == 0)
            {
                ringSpawner.mayI = true;
            }
            _scoreText.text = "" + score;
            if (score >= _highScore)
            {
                _highScore = score;
                _highScoreText.text = "" + _highScore;
            }
        }
    }
}
