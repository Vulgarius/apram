using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instruction : MonoBehaviour
{
    public GameObject instruction, items;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MenuGame());
    }
    
    private IEnumerator MenuGame()
    {
        yield return new WaitForSeconds(10);
        instruction.SetActive(true);
        instruction.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(10);
        instruction.GetComponent<FadeIn>().enabled = false;
        instruction.GetComponent<FadeIn>()._mFaded = false;
        
        instruction.GetComponent<FadeOut>().enabled = true;
        yield return new WaitForSeconds(1);
        instruction.GetComponent<FadeOut>().enabled = false;
        instruction.GetComponent<FadeOut>()._mFaded = false;
        
        instruction.SetActive(false);
        StartCoroutine(Items());
    }

    private IEnumerator Items()
    {
        items.SetActive(true);
        items.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(1);
        items.GetComponent<FadeIn>().enabled = false;
        items.GetComponent<FadeIn>()._mFaded = false;
    }
}
