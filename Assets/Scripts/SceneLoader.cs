﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private void Update()
    {
        Cursor.lockState = CursorLockMode.None;
    }

    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void Restart()
    {
        SceneManager.LoadScene("Jogo");
    }

    public void LoadVillage()
    {
        SceneManager.LoadScene("Passeio");
    }

    public void LoadRings()
    {
        SceneManager.LoadScene("Jogo");
    }
    
}
