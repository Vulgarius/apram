using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour
{

    public GameObject xWagon, xWater, xBirds, xTavern, xWood;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wagon"))
            xWagon.SetActive(true);
        else if (other.CompareTag("Water"))
            xWater.SetActive(true);
        else if (other.CompareTag("Birds"))
            xBirds.SetActive(true);
        else if (other.CompareTag("Tavern"))
            xTavern.SetActive(true);
        else if (other.CompareTag("Wood"))
            xWood.SetActive(true);
    }
}
