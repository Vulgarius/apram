﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager1 : MonoBehaviour
{
    public GameObject[] level;
    public GameObject afterPain;
    public GameObject painQuestion;
    public int selected;
    
    private int _totalPoints;
    private bool _select = false;
    
    // Update is called once per frame
    void Update()
    {
        if (_select)
        {
            Selection();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(Question());
        }
    }
    
    private void Start()
    {
        StartCoroutine(Question());
        //_totalPoints = playerBehaviour.totalPoints;
        InvokeRepeating(nameof(StartQuestion),300,300);
    }

    public void Selection()
    {
        for (int i = 0; i < 5; i++)
        {
            if (level[i] == EventSystem.current.currentSelectedGameObject)
            {
                selected = i;
                afterPain.SetActive(true);
            }
        }
    }
    
    public void StartQuestion()
    {
        StartCoroutine(Question());
    }
    private IEnumerator Question()
    {
        painQuestion.SetActive(true);
        painQuestion.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(1);
        painQuestion.GetComponent<FadeIn>().enabled = false;
        painQuestion.GetComponent<FadeIn>()._mFaded = false;

        _select = true;
    }
    
    public void ToGame()
    {
        StartCoroutine(Game());
    }
    private IEnumerator Game()
    {
        _select = false;
        afterPain.SetActive(false);
        painQuestion.GetComponent<FadeOut>().enabled = true;
        yield return new WaitForSeconds(1);
        painQuestion.GetComponent<FadeOut>().enabled = false;
        painQuestion.GetComponent<FadeOut>()._mFaded = false;
        painQuestion.SetActive(false);
    }
}