﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject[] level;
    public GameObject afterPain, exitGame;
    public GameObject painQuestion, fim;
    public GameObject buttonsAndText;
    public int selected;
    public int exitSelected;

    private int _totalPoints;
    private bool _select = false;
    private bool _exit = false;
    private int _questionCount = 0;
    private Animator _bat;
    
    float elapsedTime = 0;

    
    // Update is called once per frame
    void Update()
    {
        if (_select)
        {
            Selection();
        }

        if (_questionCount == 10)
        {
            fim.SetActive(true);
        }

        if (_exit)
        {
            Selection();
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    
    private void Start()
    {
        //Screen.orientation = ScreenOrientation.Portrait;
        //_bat = buttonsAndText.GetComponent<Animator>();
        //StartCoroutine(Question());
        //InvokeRepeating(nameof(StartQuestion),300,300);
    }

    public void QuitCommand()
    {
        StartCoroutine(Question());
        //_bat.ResetTrigger("Idle");
        _exit = true;
    }
    
    public void Selection()
    {
        for (int i = 0; i < 10; i++)
        {
            if (level[i] == EventSystem.current.currentSelectedGameObject)
            {
                
                if (_exit)
                {
                    exitSelected = i;
                    exitGame.SetActive(true);
                    _bat.SetTrigger("Move");
                }
                else
                {
                    selected = i;
                    afterPain.SetActive(true);
                    _bat.ResetTrigger("Idle");
                    _bat.SetTrigger("Move");
                }
                    
            }
        }
    }

    public void AddCount()
    {
        _questionCount++;
    }
    
    public void Quit()
    {
        SceneManager.LoadScene("BackMenu");
    }
    
    public void StartQuestion()
    {
        StartCoroutine(Question());
    }
    private IEnumerator Question()
    {
        painQuestion.SetActive(true);
        painQuestion.GetComponent<FadeIn>().enabled = true;
        yield return new WaitForSeconds(1);
        painQuestion.GetComponent<FadeIn>().enabled = false;
        painQuestion.GetComponent<FadeIn>()._mFaded = false;
        _select = true;
    }
    
    public void ToGame()
    {
        StartCoroutine(Game());
    }
    private IEnumerator Game()
    {
        _select = false;
        afterPain.SetActive(false);
        painQuestion.GetComponent<FadeOut>().enabled = true;
        yield return new WaitForSeconds(1);
        painQuestion.GetComponent<FadeOut>().enabled = false;
        painQuestion.GetComponent<FadeOut>()._mFaded = false;
        _bat.ResetTrigger("Move");
        _bat.SetTrigger("Idle");
        painQuestion.SetActive(false);
    }
    
}
